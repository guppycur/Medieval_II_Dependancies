using DMT;
using HarmonyLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace TormentedEmu_Mods_A19
{
  public class Harmony_TE_MenuMusic : IHarmony
  {
    public static string MyModFolder = "TE_MenuMusic";
    public static string MyMusicFile = "zombiemainmusic.ogg";
    public static AudioType MyMusicType = AudioType.OGGVORBIS;
    public static List<string> MusicFiles = new List<string>()
      {
        "zombiemainmusic.ogg",
        "MMIIMenuMusicDD.ogg"
      };

    public static List<AudioClip> AudioClips;
    public static BackgroundMusicMono BackgroundMusicMonoInstance;
    public static AudioSource BackgroundMusicAudioSource;
    public static int CurrentClip;
    private static bool _PlayNext;

    public void Start()
    {
      var harmony = new Harmony("TormentedEmu.Mods.A19");
      harmony.PatchAll(Assembly.GetExecutingAssembly());
    }

    [HarmonyPatch(typeof(BackgroundMusicMono), "Update")]
    public class BackgroundMusicMono_Update
    {
      static bool Prefix(ref BackgroundMusicMono __instance, ref float ___currentVolume)
      {
        if (Input.GetKeyUp(KeyCode.Space))
        {
          _PlayNext = true;
          Log.Out("Playing next menu music clip.");
        }

        if (_PlayNext)
        {
          ___currentVolume = Mathf.Clamp01(___currentVolume -= Time.deltaTime / 2.0f);
          BackgroundMusicAudioSource.volume = ___currentVolume;

          if (___currentVolume <= 0.01f)
          {
            if (CurrentClip >= MusicFiles.Count)
              CurrentClip = 0;

            BackgroundMusicAudioSource.clip = AudioClips[CurrentClip++];
            _PlayNext = false;
          }

          return false;
        }

        return true;
      }
    }

    [HarmonyPatch(typeof(BackgroundMusicMono), "Start")]
    public class BackgroundMusicMono_Start
    {
      static void Postfix(ref BackgroundMusicMono __instance, AudioSource ___audioSource)
      {
        Log.Out("TE_MenuMusic - Attempting to load the mod menu background music...");
        BackgroundMusicMonoInstance = __instance;
        BackgroundMusicAudioSource = ___audioSource;
        __instance.StartCoroutine(LoadAudioClips());
        __instance.enabled = false;
        /*
        var modMusicClip = await LoadModBackgroundMusic();
        if (modMusicClip == null)
        {
          Log.Error("TE_MenuMusic - Failed to load the mod music clip!");
          return;
        }

        Log.Out($"TE_MenuMusic - Loading new menu background music: {modMusicClip}");
        ___audioSource.clip = modMusicClip;*/
        //await Task.Delay(2000);
        //__instance.enabled = true;
      }

      static IEnumerator LoadAudioClips()
      {
        Log.Out($"TE_MenuMusic begin loading audio clips...");
        AudioClips = new List<AudioClip>(MusicFiles.Count);

        for (int i = 0; i < MusicFiles.Count; i++)
        {
          string musicPath = "file://" + Path.Combine(Utils.GetGamePath(), "Mods", MyModFolder, "Resources", MusicFiles[i]);
          UnityWebRequest webRequest = UnityWebRequestMultimedia.GetAudioClip(musicPath, MyMusicType);

          yield return webRequest.SendWebRequest();

          if (webRequest.isNetworkError || webRequest.isHttpError)
          {
            Log.Error(webRequest.error);
            Log.Error($"Failed to load audio clip: {musicPath}");
            yield break;
          }
          else
          {
            AudioClip clip = DownloadHandlerAudioClip.GetContent(webRequest);
            clip.name = MusicFiles[i];
            AudioClips.Add(clip);
            Log.Out($"Loaded clip: {MusicFiles[i]}");
            if (i == 0)
              BackgroundMusicMonoInstance.StartCoroutine(EnableMenuMusic());
          }
        }

        yield break;
      }

      static IEnumerator EnableMenuMusic()
      {
        yield return new WaitForSeconds(1.5f);

        //UnityEngine.Random.InitState(DateTime.Now.Millisecond);
        CurrentClip = 0;// UnityEngine.Random.Range(0, MusicFiles.Count);
        Log.Out($"Loading new menu music clip {CurrentClip} - {MusicFiles[CurrentClip]}");
        BackgroundMusicAudioSource.clip = AudioClips[CurrentClip++];
        BackgroundMusicMonoInstance.enabled = true;
        Log.Out("Enabling BackgroundMusicMono.");

        yield break;
      }
      static async Task<AudioClip> LoadModBackgroundMusic()
      {
        AudioClip audioClip = null;
        UnityEngine.Random.InitState(DateTime.Now.Millisecond);
        int rando = UnityEngine.Random.Range(0, MusicFiles.Count);

        string musicPath = "file://" + Path.Combine(Utils.GetGamePath(), "Mods", MyModFolder, "Resources", MusicFiles[rando]);

        using (UnityWebRequest webRequest = UnityWebRequestMultimedia.GetAudioClip(musicPath, MyMusicType))
        {
          webRequest.SendWebRequest();

          try
          {
            while (!webRequest.isDone)
              await Task.Delay(5);

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
              Log.Error($"Failed to retrieve new background music clip:  {webRequest.error}");
            }
            else
            {
              audioClip = DownloadHandlerAudioClip.GetContent(webRequest);
              audioClip.name = MusicFiles[rando];
            }
          }
          catch (Exception e)
          {
            Log.Exception(e);
          }
        }

        return audioClip;
      }
    }
  }
}
