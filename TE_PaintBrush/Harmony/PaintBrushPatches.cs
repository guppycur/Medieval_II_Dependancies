using HarmonyLib;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using DMT;
using UnityEngine;

namespace TormentedEmu_Mods_A19
{
  public class TE_PaintBrush : IHarmony
  {
    public void Start()
    {
      var harmony = new Harmony("TormentedEmu.Mods.A19");
      harmony.PatchAll(Assembly.GetExecutingAssembly());

      LoadModPainting.Init();
    }

    [HarmonyPatch(typeof(ItemActionTextureBlock), "getHitBlockFace")]
    public class ItemActionTextureBlock_getHitBlockFace
    {
      // disabled to avoid cross mesh painting
      static void Disabled_Postfix(ref int __result, ref ItemActionTextureBlock __instance, ItemActionRanged.ItemActionDataRanged _actionData, ref Vector3i blockPos, ref BlockValue bv, ref BlockFace blockFace, ref WorldRayHitInfo hitInfo)
      {
        //Log.Out("getHitBlock result: {0}, blockFace: {1}, bv: {2}", __result, blockFace, bv);

        if (__result == -1 && (int)Block.list[bv.type].MeshIndex == 10)
        {
          blockFace = BlockFace.Top;
          if (Block.list[bv.type].shape is BlockShapeNew)
          {
            blockFace = GameUtils.GetBlockFaceFromHitInfo(blockPos, bv, hitInfo.hitCollider, hitInfo.hitTriangleIdx);
          }
          if (blockFace == BlockFace.None)
          {
            return;
          }

          ChunkCluster chunkCluster = GameManager.Instance.World.ChunkClusters[hitInfo.hit.clrIdx];
          __result = chunkCluster.GetBlockFaceTexture(blockPos, blockFace);
          //Log.Out("getHitBlock after result: {0}", __result);
        }
      }
    }

    [HarmonyPatch(typeof(BlockShapeNew), "renderFace")]
    class BlockShapeNew_renderFace
    {
      /*
        118	0118	ldarg.0
        119	0119	ldfld	int32 BlockShape::MeshIndex
        120	011E	ldc.i4.s	10
        121	0120	bne.un.s	124 (0126) ldc.r4 0
        122	0122	ldloc.s	num2 (7)
        123	0124	stloc.s	num3 (8)
      */
      static int mi;
      static int id;
      static int texid;

      static FieldInfo meshIndex = AccessTools.Field(typeof(BlockShapeNew), "MeshIndex");
      static MethodInfo m_MyExtraMethod = SymbolExtensions.GetMethodInfo(() => GetTextureId(mi, id, ref texid));

      static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
      {
        var found = false;
        int count = -1;
        foreach (var instruction in instructions)
        {
          count++;

          if (instruction.Is(OpCodes.Ldc_R4, 0f))
          {
            FileLog.Log("Found OpCodes.Ldc_R4, 0f");
          }
          if (!found && count == 119)
          {
            FileLog.Log("Found Call insertion point");
            yield return new CodeInstruction(OpCodes.Ldarg_0);
            yield return new CodeInstruction(OpCodes.Ldfld, meshIndex);
            yield return new CodeInstruction(OpCodes.Ldloc_S, 7);
            yield return new CodeInstruction(OpCodes.Ldloca_S, 8);
            yield return new CodeInstruction(OpCodes.Call, m_MyExtraMethod);
            found = true;
          }
          //FileLog.Log(string.Format("Instruction OpCode: {0}, Operand: {1}", instruction.opcode, instruction.operand));

          if (instruction.LoadsField(meshIndex))
          {
            FileLog.Log("Found MeshIndex");
          }

          yield return instruction;
        }
        //if (found is false)
        //FileLog.Log("Cannot find <Stdfld someField> in OriginalType.OriginalMethod");
      }

      static void GetTextureId(int meshIndex, int id, ref int textureId)
      {
        //Log.Out("MeshIndex: {0}, My GetTextureId id: {1}, textureId: {2}", meshIndex, id, textureId);
        if (meshIndex == 10)
        {
          if (TE_BlockTextureData.list == null)
          {
            Log.Error("TE_BlockTextureData.list == null, id: {0}", id);
            textureId = 1;
            return;
          }
          
          if (TE_BlockTextureData.list[id] == null)
          {
            Log.Error("ModPaintBrush: Missing paint ID XML entry: {0}", id);
            textureId = 1;
          }
          else
          {
            textureId = (int)TE_BlockTextureData.list[id].TextureID;
          }
        }
      }
    }

    [HarmonyPatch(typeof(XUiC_ItemStack), "set_ItemStack")]
    public class XUiC_ItemStack_set_ItemStack
    {
      static AccessTools.FieldRef<XUiC_ItemStack, ItemClass> itemClass = AccessTools.FieldRefAccess<XUiC_ItemStack, ItemClass>("itemClass");
      static AccessTools.FieldRef<XUiC_ItemStack, ItemStack> itemStack = AccessTools.FieldRefAccess<XUiC_ItemStack, ItemStack>("itemStack");
      static XUiC_ItemStack _ItemStack = null;
      static MethodInfo _MyCheckMethod = SymbolExtensions.GetMethodInfo(() => CheckIfModPaintBrush(ref _ItemStack));

      // off to use transpiler method to fix flashing texture on the paint brush
      static void Disabled_Postfix(ref XUiC_ItemStack __instance, ItemStack value)
      {
        if (__instance.backgroundTexture != null)
        {
          if (itemClass(__instance) != null && itemClass(__instance).Actions[0] is ItemActionTE_TextureBlock)
          {
            //Log.Out("Postfix ItemActionTE_TextureBlock - {0}", System.Environment.StackTrace);
            __instance.backgroundTexture.IsVisible = false;
            if (itemStack(__instance).itemValue.Meta == 0)
            {
              itemStack(__instance).itemValue.Meta = (itemClass(__instance).Actions[0] as ItemActionTE_TextureBlock).DefaultTextureID;
            }
            __instance.backgroundTexture.IsVisible = true;
            MeshDescription meshDescription = MeshDescription.meshes[10];
            if (TE_BlockTextureData.list[itemStack(__instance).itemValue.Meta] == null)
            {
              Log.Out("List at {0} is null", itemStack(__instance).itemValue.Meta);
              return;
            }

            int textureID = (int)TE_BlockTextureData.list[itemStack(__instance).itemValue.Meta].TextureID;
            //Log.Out("ItemStack Postfix textureID: {0}, meta: {1}", textureID, itemStack(__instance).itemValue.Meta);
            Rect uvrect;
            if (textureID == 0)
            {
              uvrect = WorldConstants.uvRectZero;
            }
            else
            {
              if (textureID >= meshDescription.textureAtlas.uvMapping.Length)
              {
                //Log.Out("Greater than length: {0}", meshDescription.textureAtlas.uvMapping.Length);
                return;
              }

              uvrect = meshDescription.textureAtlas.uvMapping[textureID].uv;
            }
            __instance.backgroundTexture.Texture = meshDescription.textureAtlas.diffuseTexture;
            if (meshDescription.bTextureArray)
            {
              __instance.backgroundTexture.Material.SetTexture("_BumpMap", meshDescription.textureAtlas.normalTexture);
              __instance.backgroundTexture.Material.SetFloat("_Index", (float)meshDescription.textureAtlas.uvMapping[textureID].index);
              __instance.backgroundTexture.Material.SetFloat("_Size", (float)meshDescription.textureAtlas.uvMapping[textureID].blockW);
            }
            else
            {
              __instance.backgroundTexture.UVRect = uvrect;
            }
          }
        }
      }

      static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions, ILGenerator ilGenerator)
      {
        var foundFirst = false;
        var foundBranch = false;
        var next = false;
        var branchEndIndex = -1;
        Label myNewBranch = ilGenerator.DefineLabel();        
        Label label = default(Label);
        var codes = new List<CodeInstruction>(instructions);
        
        for (int i = 0; i < codes.Count; i++)
        {
          if (next && codes[i].opcode == OpCodes.Brfalse)
          {
            label = (Label)codes[i].operand;
            codes[i].operand = myNewBranch;
            next = false;
            foundBranch = true;
          }

          if (!foundFirst && codes[i].Is(OpCodes.Isinst, typeof(ItemActionTextureBlock)))
          {
            foundFirst = true;
            next = true;
          }
          
          if (foundBranch && codes[i].labels != null && codes[i].labels.Count > 0)
          {
            foreach (var l in codes[i].labels)
            {
              if (l.Equals(label))
              {
                branchEndIndex = i;
                break;
              }
            }
          }
        }

        if (branchEndIndex > -1)
        {
          var newCodes = new List<CodeInstruction>()
          {
            new CodeInstruction(OpCodes.Ldarga, 0) { labels = new List<Label>() { myNewBranch } },
            new CodeInstruction(OpCodes.Call, _MyCheckMethod),
          };

          codes.InsertRange(branchEndIndex, newCodes.AsEnumerable());
        }
        
        return codes.AsEnumerable();
      }

      static void CheckIfModPaintBrush(ref XUiC_ItemStack __instance)
      {        
        if (itemClass(__instance).Actions[0] is ItemActionTE_TextureBlock)
        {
          __instance.backgroundTexture.IsVisible = false;
          if (itemStack(__instance).itemValue.Meta == 0)
          {
            itemStack(__instance).itemValue.Meta = (itemClass(__instance).Actions[0] as ItemActionTE_TextureBlock).DefaultTextureID;
          }

          __instance.backgroundTexture.IsVisible = true;
          MeshDescription meshDescription = MeshDescription.meshes[10];
          if (TE_BlockTextureData.list[itemStack(__instance).itemValue.Meta] == null)
          {
            Log.Error("TE_BlockTextureData List at {0} is null", itemStack(__instance).itemValue.Meta);
            return;
          }

          int textureID = (int)TE_BlockTextureData.list[itemStack(__instance).itemValue.Meta].TextureID;
          Rect uvrect;
          if (textureID == 0)
          {
            uvrect = WorldConstants.uvRectZero;
          }
          else
          {
            if (textureID >= meshDescription.textureAtlas.uvMapping.Length)
            {
              return;
            }

            uvrect = meshDescription.textureAtlas.uvMapping[textureID].uv;
          }

          __instance.backgroundTexture.Texture = meshDescription.textureAtlas.diffuseTexture;
          if (meshDescription.bTextureArray)
          {
            __instance.backgroundTexture.Material.SetTexture("_BumpMap", meshDescription.textureAtlas.normalTexture);
            __instance.backgroundTexture.Material.SetFloat("_Index", (float)meshDescription.textureAtlas.uvMapping[textureID].index);
            __instance.backgroundTexture.Material.SetFloat("_Size", (float)meshDescription.textureAtlas.uvMapping[textureID].blockW);
          }
          else
          {
            __instance.backgroundTexture.UVRect = uvrect;
          }
        }        
      }
    }
  }
}