using SDX.Compiler;
using Mono.Cecil;
using System.Linq;
using DMT;

public class PatchPaintingDesc : IPatcherMod
{
  public bool Patch(ModuleDefinition module)
  {
    Log("Patch BlockShapeNew start...");
    /*
    var gm = module.Types.First(d => d.Name == "BlockShapeNew");

    foreach (var m in gm.Methods)
    {
      SetMethodToPublic(m);
    }

    foreach (var f in gm.Fields)
    {
      SetFieldToPublic(f);
    }

    var msm = gm.NestedTypes.First(n => n.Name == "MySimpleMesh");
    msm.IsPublic = true;

    foreach (var sc in gm.NestedTypes)
    {
      Log("Nested type: {0}, IsNotPublic: {1}, IsPublic: {2}", sc, sc.IsNotPublic, sc.IsPublic);
      //sc.IsNotPublic = false;
      //sc.IsPublic = true;
    }
    */
    return true;
  }

  public void Log(string msg, params object[] args)
  {
    Logging.Log(string.Format(msg, args));
  }

  public bool Link(ModuleDefinition gameModule, ModuleDefinition modModule)
  {
    
    //var gm = gameModule.Types.First(d => d.Name == "BlockShapeNew");
    //var renderFace = gm.Methods.First(m => m.Name == "renderFace");

    //var emuLoadHook = gameModule.ImportReference(modModule.Types.First(c => c.Name == "TE_Utils").Resolve().Methods.First(m => m.Name == "Check"));
    //foreach (var i in renderFace.Body.Instructions)
    //{
      //Log("OpCode: {0}, Operand: {1}", i.OpCode, i.Operand);
    //}
    
    return true;
  }


  // Helper functions to allow us to access and change variables that are otherwise unavailable.
  private void SetMethodToVirtual(MethodDefinition meth)
  {
    meth.IsVirtual = true;
  }

  private void SetFieldToPublic(FieldDefinition field)
  {
    field.IsFamily = false;
    field.IsPrivate = false;
    field.IsPublic = true;
  }
  private void SetMethodToPublic(MethodDefinition field)
  {
    field.IsFamily = false;
    field.IsPrivate = false;
    field.IsPublic = true;
  }
}
