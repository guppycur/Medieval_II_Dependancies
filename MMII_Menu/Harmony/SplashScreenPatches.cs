﻿using HarmonyLib;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

namespace TormentedEmu_Mods_A19
{
  public class Harmony_MMII_SplashScreenPatches
  {
    static TE_IntroSplashScript _IntroSplashScript;

    [HarmonyPatch(typeof(SplashScreenScript), "Awake")]
    public class SplashScreenScript_Awake
    {
      static void Postfix(ref SplashScreenScript __instance, ref float ___timeToLoadLevel)
      {
        ___timeToLoadLevel = 3.0f;
        __instance.wdwSplashScreen.gameObject.SetActive(false);
        Log.Out("Loading new IntroSplash");
        GameObject go = new GameObject("IntroSplashGO");
        _IntroSplashScript = go.AddComponent<TE_IntroSplashScript>();
      }

    }

    [HarmonyPatch(typeof(SplashScreenScript), "OnEnable")]
    public class SplashScreenScript_OnEnable
    {
      static bool Prefix(SplashScreenScript __instance)
      {
        __instance.wdwSplashScreen.gameObject.SetActive(false);
        //__instance.gameObject.AddComponent<SplashScreenAudio>();
        return false;
      }
    }

    [HarmonyPatch(typeof(SplashScreenScript), "Update")]
    public class SplashScreenScript_Update
    {
      enum SplashState
      {
        SPLASH_VISIBLE,
        FADEOUT_STARTED,
        FADEOUT_DONE
      }

      static bool Prefix(ref SplashScreenScript __instance, ref int ___state, ref UILabel ___lblLoading, ref float ___timeToLoadLevel, ref bool ___precacheRunning, ref float ___precacheStatus)
      {
        switch ((SplashState)___state)
        {
          case SplashState.SPLASH_VISIBLE:
            if (Input.anyKey || Time.timeSinceLevelLoad > 8f)
            {
              Log.Out($"Time.timeSinceLevelLoad > 8f: {Time.timeSinceLevelLoad > 8f},  time: {Time.timeSinceLevelLoad}, anyKey: {Input.anyKey}");


              if (_IntroSplashScript != null)
              {
                _IntroSplashScript.StartFadeOut();
              }

              ___state = (int)SplashState.FADEOUT_STARTED;
            }
            break;

          case SplashState.FADEOUT_STARTED:
            if (___timeToLoadLevel > 0f)
            {
              ___timeToLoadLevel -= Time.deltaTime;

              if (___timeToLoadLevel <= 1f)
              {
                if (__instance.transformLoading != null && !__instance.transformLoading.gameObject.activeSelf)
                {
                  __instance.transformLoading.gameObject.SetActive(value: true);
                }
              }

              if (___timeToLoadLevel <= 0f)
              {
                ___state = (int)SplashState.FADEOUT_DONE;
              }
            }
            break;

          case SplashState.FADEOUT_DONE:
            if (!___precacheRunning)
            {
              Log.Out("Loading main scene");
              SceneManager.LoadScene("SceneGame");
            }
            break;

          default:
            throw new ArgumentOutOfRangeException();
        }

        if (___lblLoading != null)
        {
          if (___precacheRunning)
          {
            ___lblLoading.text = string.Format(Localization.Get("splashscreenPrecaching"), ___precacheStatus);
          }
          else
          {
            ___lblLoading.text = "Tossing coins to your Witcher..."; // Localization.Get("splashscreenLoadMainMenu");
          }
        }

        return false;
      }
    }

  }

}