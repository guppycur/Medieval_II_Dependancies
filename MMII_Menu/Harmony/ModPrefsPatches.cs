﻿using HarmonyLib;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TormentedEmu_Mods_A19
{
  public class Harmony_MMII_ModPrefsPatches
  {
    [HarmonyPatch(typeof(EntityPlayerLocal), "guiDrawCrosshair")]
    public class EntityPlayerLocal_guiDrawCrosshair
    {
      static IEnumerable<CodeInstruction> OffTranspiler(IEnumerable<CodeInstruction> instructions)
      {
        var codes = new List<CodeInstruction>(instructions);

        return codes.AsEnumerable();
      }

      static bool OffPrefix(ref EntityPlayerLocal __instance)
      {
        if (ModPrefs.Instance.CrosshairEnabled)
          return true;

        return false;
      }

      static bool Prefix(ref EntityPlayerLocal __instance, ref NGuiWdwInGameHUD _guiInGame, bool bModalWindowOpen, ref NGUIWindowManager ___nguiWindowManager)
      {
        if (!ModPrefs.Instance.CrosshairEnabled || Event.current.type != EventType.Repaint || __instance.IsDead() || __instance.AttachedToEntity != null)
        {
          return false;
        }

        ItemClass.EnumCrosshairType crosshairType = __instance.inventory.holdingItem.GetCrosshairType(__instance.inventory.holdingItemData);
        if (bModalWindowOpen)
        {
          return false;
        }

        ___nguiWindowManager.Show(EnumNGUIWindow.StealthIndicator, _bEnable: false);
        if (__instance.inventory == null)
        {
          return false;
        }

        Vector2 crosshairPosition2D = __instance.GetCrosshairPosition2D();
        crosshairPosition2D.y = (float)Screen.height - crosshairPosition2D.y;
        Color modColor = ModPrefs.Instance.CrosshairColor;

        switch (crosshairType)
        {
          case ItemClass.EnumCrosshairType.Crosshair:
            if (__instance.AimingGun && !ItemAction.ShowDistanceDebugInfo)
            {
              break;
            }
            goto case ItemClass.EnumCrosshairType.CrosshairOnAiming;

          case ItemClass.EnumCrosshairType.CrosshairOnAiming:
            {
              __instance.GetCrosshairOpenArea();
              float value = EffectManager.GetValue(PassiveEffects.SpreadDegreesHorizontal, __instance.inventory.holdingItemData.itemValue, 90f, __instance);
              value *= 0.5f;
              value *= (__instance.inventory.holdingItemData.actionData[0] as ItemActionRanged.ItemActionDataRanged).lastAccuracy;
              value *= (float)Mathf.RoundToInt((float)Screen.width / __instance.cameraTransform.GetComponent<Camera>().fieldOfView);
              float value2 = EffectManager.GetValue(PassiveEffects.SpreadDegreesVertical, __instance.inventory.holdingItemData.itemValue, 90f, __instance);
              value2 *= 0.5f;
              value2 *= (__instance.inventory.holdingItemData.actionData[0] as ItemActionRanged.ItemActionDataRanged).lastAccuracy;
              value2 *= (float)Mathf.RoundToInt((float)Screen.width / __instance.cameraTransform.GetComponent<Camera>().fieldOfView);
              int num = (int)crosshairPosition2D.x;
              int num2 = (int)crosshairPosition2D.y;
              int num3 = 18;
              Color black = Color.black;
              Color white = modColor;
              black.a = 1.0f * __instance.weaponCrossHairAlpha;
              white.a = 1.0f * __instance.weaponCrossHairAlpha;

              GUIUtils.DrawLine(new Vector2((float)num - value, num2 + 1), new Vector2((float)num - (value + (float)num3), num2 + 1), black);
              GUIUtils.DrawLine(new Vector2((float)num + value, num2 + 1), new Vector2((float)num + value + (float)num3, num2 + 1), black);
              GUIUtils.DrawLine(new Vector2(num + 1, (float)num2 - value2), new Vector2(num + 1, (float)num2 - (value2 + (float)num3)), black);
              GUIUtils.DrawLine(new Vector2(num + 1, (float)num2 + value2), new Vector2(num + 1, (float)num2 + value2 + (float)num3), black);
              GUIUtils.DrawLine(new Vector2((float)num + value, num2), new Vector2((float)num + value + (float)num3, num2), white);
              GUIUtils.DrawLine(new Vector2(num, (float)num2 - value2), new Vector2(num, (float)num2 - (value2 + (float)num3)), white);
              GUIUtils.DrawLine(new Vector2((float)num - value, num2), new Vector2((float)num - (value + (float)num3), num2), white);
              GUIUtils.DrawLine(new Vector2(num, (float)num2 + value2), new Vector2(num, (float)num2 + value2 + (float)num3), white);
              GUIUtils.DrawLine(new Vector2((float)num - value, num2 - 1), new Vector2((float)num - (value + (float)num3), num2 - 1), black);
              GUIUtils.DrawLine(new Vector2((float)num + value, num2 - 1), new Vector2((float)num + value + (float)num3, num2 - 1), black);
              GUIUtils.DrawLine(new Vector2(num - 1, (float)num2 - value2), new Vector2(num - 1, (float)num2 - (value2 + (float)num3)), black);
              GUIUtils.DrawLine(new Vector2(num - 1, (float)num2 + value2), new Vector2(num - 1, (float)num2 + value2 + (float)num3), black);
              break;
            }

          case ItemClass.EnumCrosshairType.Plus:
            if (Event.current.type == EventType.Repaint)
            {
              Color color2 = GUI.color;
              GUI.color = new Color(modColor.r, modColor.g, modColor.b, _guiInGame.crosshairAlpha);
              GUI.DrawTexture(new Rect(crosshairPosition2D.x - (float)(_guiInGame.CrosshairTexture.width / 2), crosshairPosition2D.y - (float)(_guiInGame.CrosshairTexture.height / 2), _guiInGame.CrosshairTexture.width, _guiInGame.CrosshairTexture.height), _guiInGame.CrosshairTexture);
              GUI.color = color2;
            }
            break;

          case ItemClass.EnumCrosshairType.Damage:
            if (Event.current.type == EventType.Repaint)
            {
              Color color7 = GUI.color;

              if (__instance.PlayerUI.xui.BackgroundGlobalOpacity < 1f)
              {
                float a6 = color7.a * __instance.PlayerUI.xui.BackgroundGlobalOpacity;
                GUI.color = new Color(modColor.r, modColor.g, modColor.b, a6);
              }
              else
              {
                GUI.color = new Color(modColor.r, modColor.g, modColor.b, 1.0f);
              }

              GUI.DrawTexture(new Rect(crosshairPosition2D.x - 22f, crosshairPosition2D.y - 22f, 44f, 44f), _guiInGame.CrosshairDamage);
              GUI.color = color7;
            }
            break;

          case ItemClass.EnumCrosshairType.Repair:
            if (Event.current.type == EventType.Repaint)
            {
              Color color5 = GUI.color;

              if (__instance.PlayerUI.xui.BackgroundGlobalOpacity < 1f)
              {
                float a4 = color5.a * __instance.PlayerUI.xui.BackgroundGlobalOpacity;
                GUI.color = new Color(modColor.r, modColor.g, modColor.b, a4);
              }
              else
              {
                GUI.color = new Color(modColor.r, modColor.g, modColor.b, 1.0f);
              }

              GUI.DrawTexture(new Rect(crosshairPosition2D.x - 22f, crosshairPosition2D.y - 22f, 44f, 44f), _guiInGame.CrosshairRepair);

              GUI.color = color5;
            }
            break;

          case ItemClass.EnumCrosshairType.Upgrade:
            if (Event.current.type == EventType.Repaint)
            {
              Color color6 = GUI.color;

              if (__instance.PlayerUI.xui.BackgroundGlobalOpacity < 1f)
              {
                float a5 = color6.a * __instance.PlayerUI.xui.BackgroundGlobalOpacity;
                GUI.color = new Color(modColor.r, modColor.g, modColor.b, a5);
              }
              else
              {
                GUI.color = new Color(modColor.r, modColor.g, modColor.b, 1.0f);
              }

              GUI.DrawTexture(new Rect(crosshairPosition2D.x - 22f, crosshairPosition2D.y - 22f, 44f, 44f), _guiInGame.CrosshairUpgrade);
              GUI.color = color6;
            }
            break;

          case ItemClass.EnumCrosshairType.Heal:
            if (Event.current.type == EventType.Repaint)
            {
              Color color3 = GUI.color;

              if (__instance.PlayerUI.xui.BackgroundGlobalOpacity < 1f)
              {
                float a2 = color3.a * __instance.PlayerUI.xui.BackgroundGlobalOpacity;
                GUI.color = new Color(modColor.r, modColor.g, modColor.b, a2);
              }
              else
              {
                GUI.color = new Color(modColor.r, modColor.g, modColor.b, 1.0f);
              }

              GUI.DrawTexture(new Rect(crosshairPosition2D.x - 22f, crosshairPosition2D.y - 22f, 44f, 44f), _guiInGame.CrosshairRepair);
              GUI.color = color3;
            }
            break;

          case ItemClass.EnumCrosshairType.PowerItem:
            if (Event.current.type == EventType.Repaint)
            {
              Color color4 = GUI.color;

              if (__instance.PlayerUI.xui.BackgroundGlobalOpacity < 1f)
              {
                float a3 = color4.a * __instance.PlayerUI.xui.BackgroundGlobalOpacity;
                GUI.color = new Color(modColor.r, modColor.g, modColor.b, a3);
              }
              else
              {
                GUI.color = new Color(modColor.r, modColor.g, modColor.b, 1.0f);
              }

              GUI.DrawTexture(new Rect(crosshairPosition2D.x - 22f, crosshairPosition2D.y - 22f, 44f, 44f), _guiInGame.CrosshairPowerItem);
              GUI.color = color4;
            }
            break;

          case ItemClass.EnumCrosshairType.PowerSource:
            if (Event.current.type == EventType.Repaint)
            {
              Color color = GUI.color;

              if (__instance.PlayerUI.xui.BackgroundGlobalOpacity < 1f)
              {
                float a = color.a * __instance.PlayerUI.xui.BackgroundGlobalOpacity;
                GUI.color = new Color(modColor.r, modColor.g, modColor.b, a);
              }
              else
              {
                GUI.color = new Color(modColor.r, modColor.g, modColor.b, 1.0f);
              }

              GUI.DrawTexture(new Rect(crosshairPosition2D.x - 22f, crosshairPosition2D.y - 22f, 44f, 44f), _guiInGame.CrosshairPowerSource);
              GUI.color = color;
            }
            break;
        }

        return false;
      }
    }

  }
}
