﻿using DMT;
using HarmonyLib;
using System.Reflection;

namespace TormentedEmu_Mods_A19
{
  public class TE_MMII_MenuDmtInit : IHarmony
  {
    public void Start()
    {
      var harmony = new Harmony("TormentedEmu.MMII.MenuDmt.Mods.A19");
      harmony.PatchAll(Assembly.GetExecutingAssembly());
    }
  }
}
