﻿using DMT;
using HarmonyLib;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace TormentedEmu_Mods_A19
{
  public class Harmony_MMII_WindowManagerPatch : IHarmony
  {
    public static Dictionary<string, GUIWindow> _GuiWindowDict;

    public void Start()
    {
      var harmony = new Harmony("TormentedEmu.MMII.MenuDmt.Mods.A19");
      var xuiUpdater = AccessTools.TypeByName("XUiUpdater");
      var addMethod = AccessTools.Method(xuiUpdater, "Add");
      var mPostfix = SymbolExtensions.GetMethodInfo(() => XUiUpdater_Add_Postfix());
      harmony.Patch(addMethod, null, new HarmonyMethod(mPostfix));
    }

    public static void XUiUpdater_Add_Postfix()
    {
      if (_GuiWindowDict != null)
      {
        if (_GuiWindowDict.ContainsKey("MMII_MainMenu"))
        {
          _GuiWindowDict[XUiC_MainMenu.ID] = _GuiWindowDict["MMII_MainMenu"];
        }

        if (_GuiWindowDict.ContainsKey("MMII_InGameMenu"))
        {
          _GuiWindowDict[XUiC_InGameMenuWindow.ID] = _GuiWindowDict["MMII_InGameMenu"];
        }
      }
    }

    [HarmonyPatch(typeof(GUIWindowManager), "Awake")]
    public class GUIWindowManager_Awake
    {
      static void Postfix(ref GUIWindowManager __instance, ref Dictionary<string, GUIWindow> ___nameToWindowMap)
      {
        _GuiWindowDict = ___nameToWindowMap;
      }
    }

    [HarmonyPatch(typeof(NGUIWindowManager), "Awake")]
    public class NGUIWindowManager_Awake
    {
      static void Postfix(ref NGUIWindowManager __instance, Dictionary<EnumNGUIWindow, Transform> ___windowMap)
      {
        if (__instance.transform.parent == null)
          return;

        Transform mainMenuBackground = ___windowMap[EnumNGUIWindow.MainMenuBackground];
        UITexture[] uiTexs = mainMenuBackground.GetComponentsInChildren<UITexture>();
        uiTexs[0].mainTexture = LoadTexture("testbackground1.png");
        uiTexs[1].mainTexture = LoadTexture("mainmenulogo.png");
      }

      static Texture2D LoadTexture(string fileName)
      {
        string filePath = Path.GetFullPath(Path.Combine(Utils.GetGamePath(), "Mods", "MMII_Menu", "Textures", fileName));
        Texture2D tex = new Texture2D(2, 2);
        ImageConversion.LoadImage(tex, File.ReadAllBytes(filePath), false);
        tex.name = fileName;
        return tex;
      }
    }
  }
}
