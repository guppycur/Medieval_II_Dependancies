﻿using HarmonyLib;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using UnityEngine;

namespace TormentedEmu_Mods_A19
{
  public class Harmony_MMII_CreditsPatch
  {

    [HarmonyPatch(typeof(XUiC_Credits), "LoadCredits")]
    public class XUiC_Credits_LoadCredits
    {
      struct ModCreditsInfo
      {
        public string Name;
        public string Contribution;
        public string CenterText;
      }

      private static Dictionary<string, List<ModCreditsInfo>> credits;
      private static List<string> categories;

      static bool Prefix(ref XUiC_Credits __instance, ref Transform ___creditsGrid, ref GameObject ___headerRowTemplate, ref GameObject ___normalRowTemplate)
      {
        if (credits != null)
        {
          return false;
        }

        credits = new Dictionary<string, List<ModCreditsInfo>>();
        categories = new List<string>();
        XmlDocument xmlDocument = new XmlDocument();
        string filePath = Path.GetFullPath(Path.Combine(Utils.GetGamePath(), "Mods", "MMII_Menu", "Config", "ModCredits.xml"));
        xmlDocument.Load(filePath);

        foreach (XmlNode childNode in xmlDocument.SelectNodes("//credits")[0].ChildNodes)
        {
          if (childNode.NodeType == XmlNodeType.Comment)
            continue;

          string value = childNode.Attributes["name"].Value;
          List<ModCreditsInfo> list = new List<ModCreditsInfo>();

          foreach (XmlNode childNode2 in childNode.ChildNodes)
          {
            ModCreditsInfo item = default(ModCreditsInfo);
            if (childNode2.Attributes["name"] != null)
            {
              item.Name = childNode2.Attributes["name"].Value;
            }
            else
            {
              item.Name = "";
            }
            if (childNode2.Attributes["center_text"] != null)
            {
              item.CenterText = childNode2.Attributes["center_text"].Value;
            }
            else
            {
              item.CenterText = "";
            }
            if (childNode2.Attributes["contribution"] != null)
            {
              item.Contribution = childNode2.Attributes["contribution"].Value;
            }
            else
            {
              item.Contribution = "";
            }
            list.Add(item);
          }

          categories.Add(value);
          credits.Add(value, list);
        }

        int num = 1000;

        for (int i = 0; i < categories.Count; i++)
        {
          num++;
          string text = categories[i];
          GameObject gameObject = Object.Instantiate(___headerRowTemplate);
          gameObject.name = num + gameObject.name;
          gameObject.transform.parent = ___creditsGrid.transform;
          gameObject.transform.localScale = Vector3.one;
          gameObject.transform.localPosition = Vector3.zero;
          gameObject.transform.Find("caption").GetComponent<UILabel>().text = text;
          gameObject.SetActive(value: true);

          foreach (ModCreditsInfo item2 in credits[text])
          {
            num++;
            GameObject gameObject2 = Object.Instantiate(___normalRowTemplate);
            gameObject2.name = num + gameObject2.name;
            gameObject2.transform.parent = ___creditsGrid.transform;
            gameObject2.transform.localScale = Vector3.one;
            gameObject2.transform.localPosition = Vector3.zero;
            gameObject2.transform.Find("name").GetComponent<UILabel>().text = item2.Name;
            gameObject2.transform.Find("contribution").GetComponent<UILabel>().text = item2.Contribution;
            gameObject2.transform.Find("centertext").GetComponent<UILabel>().text = item2.CenterText;
            gameObject2.transform.Find("line").gameObject.SetActive(item2.Name.Length > 0 && item2.Contribution.Length > 0);
            gameObject2.SetActive(value: true);
          }

          num++;
          GameObject gameObject3 = Object.Instantiate(___normalRowTemplate);
          gameObject3.name = num + gameObject3.name;
          gameObject3.transform.parent = ___creditsGrid.transform;
          gameObject3.transform.localScale = Vector3.one;
          gameObject3.transform.localPosition = Vector3.zero;
          gameObject3.transform.Find("name").GetComponent<UILabel>().text = "";
          gameObject3.transform.Find("contribution").GetComponent<UILabel>().text = "";
          gameObject3.transform.Find("centertext").GetComponent<UILabel>().text = "";
          gameObject3.transform.Find("line").gameObject.SetActive(value: false);
          gameObject3.SetActive(value: true);
        }

        ___creditsGrid.GetComponent<UIGrid>().Reposition();

        return false;
      }
    }
  }
}
