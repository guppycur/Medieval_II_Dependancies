﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class TE_IntroSplashScript : MonoBehaviour
{
  private Material _Mat;
  private Animator _Animator;
  private Transform _IntroSplash;
  private float _TargetAlpha = 1f;
  private float _TargetFactor = 0.0005f;
  private bool _IsFading = false;

  private void Awake()
  {
    StartCoroutine(LoadBundle());
    enabled = false;
  }

  void Update()
  {
    if (_Mat == null)
      return;

    _Mat.color = Color.Lerp(_Mat.color, new Color(1f, 1f, 1f, _TargetAlpha), _TargetFactor);

    if (_IsFading)
    {
      _IntroSplash.transform.position += (Vector3.back * Time.deltaTime * 10);
    }
  }

  public void StartFadeOut()
  {
    _TargetAlpha = 0f;
    _TargetFactor = 0.001f;
    _Animator.enabled = false;
    _IsFading = true;
  }

  IEnumerator LoadBundle()
  {
    string bundlePath = "file://" + Path.Combine(Utils.GetGamePath(), "Mods", "MMII_Menu", "Resources", "IntroSplash.unity3d");

    var uwr = UnityWebRequestAssetBundle.GetAssetBundle(bundlePath);
    yield return uwr.SendWebRequest();

    AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);
    var loadAsset = bundle.LoadAssetAsync<GameObject>("assets/materials/IntroSplash.prefab");
    yield return loadAsset;

    //string filePath = Path.GetFullPath(Path.Combine(Utils.GetGamePath(), "Mods", "MMII_Menu", "Resources", "IntroSplash.unity3d"));
    //string filePath = Path.GetFullPath(Path.Combine(Utils.GetGamePath(), "Mods", "TE_ExperiMental", "Resources", "Splashbundle"));
    //AssetBundleManager.Instance.LoadAssetBundle(filePath, false);

    GameObject go = Instantiate(loadAsset.asset) as GameObject;
    go.transform.parent = transform;
    _IntroSplash = go.transform;
    var renderer = go.GetComponent<Renderer>();
    _Mat = renderer.material;
    _Mat.color = new Color(1f, 1f, 1f, 0.2f);
    _Animator = go.GetComponent<Animator>();
    enabled = true;
    Log.Out("Finished loading new IntroSplash bundle.");
  }
}
