//using Steamworks;

public class XUiC_MMII_InGameMenuWindow : XUiController
{
	public static string ID = "MMII_InGameMenu";

	private XUiC_SimpleButton btnInvite;
	private XUiC_SimpleButton btnOptions;
	private XUiC_SimpleButton btnHelp;
	private XUiC_SimpleButton btnSave;
	private XUiC_SimpleButton btnExit;
	private XUiC_SimpleButton btnExportPrefab;
	private XUiC_SimpleButton btnTpPoi;
  private XUiC_SimpleButton btnMMIIOptions;
  private bool continueGamePause;

	private static readonly string ServerInfoWindowGroupName = "serverinfowindow";

	private string serverInfoWindowGroup;

	public override void Init()
	{
    Log.Out($"XUiC_MMII_InGameMenuWindow::Init");

		base.Init();

		ID = base.WindowGroup.ID;
		btnInvite = GetChildById("btnInvite").GetChildByType<XUiC_SimpleButton>();
		btnInvite.OnPressed += BtnInvite_OnPressed;
		btnOptions = GetChildById("btnOptions").GetChildByType<XUiC_SimpleButton>();
		btnOptions.OnPressed += BtnOptions_OnPressed;
		btnHelp = GetChildById("btnHelp").GetChildByType<XUiC_SimpleButton>();
		btnHelp.OnPressed += BtnHelp_OnPressed;
		btnSave = GetChildById("btnSave").GetChildByType<XUiC_SimpleButton>();
		btnSave.OnPressed += BtnSave_OnPressed;
		btnExit = GetChildById("btnExit").GetChildByType<XUiC_SimpleButton>();
		btnExit.OnPressed += BtnExit_OnPressed;
		btnExportPrefab = GetChildById("btnExportPrefab").GetChildByType<XUiC_SimpleButton>();
		btnExportPrefab.OnPressed += BtnExportPrefab_OnPressed;
		btnTpPoi = GetChildById("btnTpPoi").GetChildByType<XUiC_SimpleButton>();
		btnTpPoi.OnPressed += BtnTpPoi_OnPressed;

    btnMMIIOptions = GetChildById("btnMMIIOptions").GetChildByType<XUiC_SimpleButton>();
    btnMMIIOptions.OnPressed += BtnMMIIOptions_OnPressed;

    XUiController xUiController = base.xui.FindWindowGroupByName(ServerInfoWindowGroupName);
		if (xUiController != null)
		{
			serverInfoWindowGroup = xUiController.WindowGroup.ID;
		}
	}

	private void BtnInvite_OnPressed(XUiController _sender, OnPressEventArgs _e)
	{
		ulong lobbyId = Steam.Masterserver.Lobby.LobbyId;
		base.xui.playerUI.windowManager.Close(windowGroup.ID);
		//SteamFriends.ActivateGameOverlayInviteDialog(new CSteamID(lobbyId));
	}

	private void BtnOptions_OnPressed(XUiController _sender, OnPressEventArgs _e)
	{
		continueGamePause = true;
		base.xui.playerUI.windowManager.Close(windowGroup.ID);
		LocalPlayerUI.primaryUI.windowManager.Open(XUiC_OptionsMenu.ID, _bModal: true);
	}

  private void BtnMMIIOptions_OnPressed(XUiController _sender, OnPressEventArgs _e)
  {
    continueGamePause = true;
    base.xui.playerUI.windowManager.Close(windowGroup.ID);
    LocalPlayerUI.primaryUI.windowManager.Open(XUiC_MMII_ModOptions.ID, _bModal: true);
  }

  private void BtnHelp_OnPressed(XUiController _sender, OnPressEventArgs _onPressEventArgs)
	{
		base.xui.playerUI.windowManager.Close(windowGroup.ID);
		base.xui.playerUI.windowManager.Open(XUiC_PrefabEditorHelp.ID, _bModal: true);
	}

	private void BtnSave_OnPressed(XUiController _sender, OnPressEventArgs _e)
	{
		if (PrefabEditModeManager.Instance.IsActive())
		{
			XUiC_PrefabList.SavePrefab(base.xui, savePrefab);
			return;
		}
		GameManager.Instance.SaveLocalPlayerData();
		GameManager.Instance.SaveWorld();
	}

	private void savePrefab(XUiC_SaveDirtyPrefab.ESelectedAction _action)
	{
		base.xui.playerUI.windowManager.Open(ID, _bModal: true);
	}

	private void BtnExit_OnPressed(XUiController _sender, OnPressEventArgs _e)
	{
		if (PrefabEditModeManager.Instance.IsActive() && SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer && PrefabEditModeManager.Instance.VoxelPrefab != null && PrefabEditModeManager.Instance.NeedsSaving)
		{
			XUiC_SaveDirtyPrefab.Show(base.xui, exitGame);
		}
		else
		{
			exitGame(XUiC_SaveDirtyPrefab.ESelectedAction.DontSave);
		}
	}

	private void exitGame(XUiC_SaveDirtyPrefab.ESelectedAction _action)
	{
		if (_action == XUiC_SaveDirtyPrefab.ESelectedAction.Cancel)
		{
			base.xui.playerUI.windowManager.Open(ID, _bModal: true);
			return;
		}
		GameManager.Instance.Disconnect();
		GameManager.Instance.SetActiveBlockTool(null);
	}

	private void BtnExportPrefab_OnPressed(XUiController _sender, OnPressEventArgs _e)
	{
		base.xui.playerUI.windowManager.Close(windowGroup.ID);
		base.xui.playerUI.windowManager.Open(XUiC_ExportPrefab.ID, _bModal: true);
	}

	private void BtnTpPoi_OnPressed(XUiController _sender, OnPressEventArgs _e)
	{
		base.xui.playerUI.windowManager.Close(windowGroup.ID);
		base.xui.playerUI.windowManager.Open(XUiC_PoiTeleportMenu.ID, _bModal: true);
	}

	public override void OnOpen()
	{
		base.OnOpen();
		int num = SingletonMonoBehaviour<ConnectionManager>.Instance.ClientCount() + ((!GameManager.IsDedicatedServer) ? 1 : 0);
		int @int = GamePrefs.GetInt(EnumGamePrefs.ServerMaxPlayerCount);
		btnInvite.Enabled = (Steam.Masterserver.Lobby.IsInLobby && (!SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer || num < @int));
		btnSave.ViewComponent.IsVisible = (GameManager.Instance.IsEditMode() && SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer);
		btnHelp.ViewComponent.IsVisible = GameManager.Instance.IsEditMode();
		btnExportPrefab.ViewComponent.IsVisible = (!GameManager.Instance.IsEditMode() && GamePrefs.GetBool(EnumGamePrefs.DebugMenuEnabled));
		btnTpPoi.ViewComponent.IsVisible = (GameManager.Instance.IsEditMode() || GamePrefs.GetBool(EnumGamePrefs.DebugMenuEnabled));
		continueGamePause = false;
		GameManager.Instance.Pause(_bOn: true);
		base.xui.playerUI.nguiWindowManager.Close(EnumNGUIWindow.FocusHealthState);
		base.xui.playerUI.windowManager.Close("compass");
		if (GameManager.Instance.IsPaused())
		{
			base.xui.playerUI.windowManager.Close("toolbelt");
		}
		if (GamePrefs.GetBool(EnumGamePrefs.DebugMenuEnabled))
		{
			base.xui.playerUI.windowManager.Open(XUiC_EditorPanelSelector.ID, _bModal: false);
		}
		if (serverInfoWindowGroup != null)
		{
			base.xui.playerUI.windowManager.Open(serverInfoWindowGroup, _bModal: false);
		}
	}

	public override void OnClose()
	{
		base.OnClose();
		base.xui.playerUI.windowManager.Close(XUiC_EditorPanelSelector.ID);
		if (!continueGamePause)
		{
			GameManager.Instance.Pause(_bOn: false);
		}
		if (serverInfoWindowGroup != null)
		{
			base.xui.playerUI.windowManager.Close(serverInfoWindowGroup);
		}
	}

	public override void Update(float _dt)
	{
		base.Update(_dt);
		if (PrefabEditModeManager.Instance.IsActive())
		{
			bool enabled = PrefabEditModeManager.Instance.VoxelPrefab != null;
			btnSave.Enabled = enabled;
		}
		btnExportPrefab.Enabled = BlockToolSelection.Instance.SelectionActive;
	}
}
