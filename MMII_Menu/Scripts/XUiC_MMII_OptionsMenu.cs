public class XUiC_MMII_OptionsMenu : XUiController
{
  public static string ID = "MMII_OptionsMenu";

  private bool continueGamePause;

  public override void Init()
  {
    base.Init();

    ID = WindowGroup.ID;
    (GetChildById("btnVideo") as XUiC_SimpleButton).OnPressed += btnVideo_OnPressed;
    (GetChildById("btnAudio") as XUiC_SimpleButton).OnPressed += btnAudio_OnPressed;
    (GetChildById("btnControls") as XUiC_SimpleButton).OnPressed += btnControls_OnPressed;
    (GetChildById("btnProfiles") as XUiC_SimpleButton).OnPressed += btnProfiles_OnPressed;
    (GetChildById("btnAccount") as XUiC_SimpleButton).OnPressed += btnAccount_OnPressed;
    XUiController[] childrenById = GetChildrenById("btnBack");
    for (int i = 0; i < childrenById.Length; i++)
    {
      ((XUiC_SimpleButton)childrenById[i]).OnPressed += btnBack_OnPressed;
    }
  }

  private void btnVideo_OnPressed(XUiController _sender, OnPressEventArgs _e)
  {
    OpenOptions(XUiC_OptionsVideo.ID);
  }

  private void btnAudio_OnPressed(XUiController _sender, OnPressEventArgs _e)
  {
    OpenOptions(XUiC_OptionsAudio.ID);
  }

  private void btnControls_OnPressed(XUiController _sender, OnPressEventArgs _e)
  {
    OpenOptions(XUiC_OptionsControls.ID);
  }

  private void btnProfiles_OnPressed(XUiController _sender, OnPressEventArgs _e)
  {
    OpenOptions(XUiC_OptionsProfiles.ID);
  }

  private void btnAccount_OnPressed(XUiController _sender, OnPressEventArgs _e)
  {
    OpenOptions(XUiC_OptionsUsername.ID);
  }

  private void btnBack_OnPressed(XUiController _sender, OnPressEventArgs _e)
  {
    base.xui.playerUI.windowManager.Close(windowGroup.ID);
    if (GameStats.GetInt(EnumGameStats.GameState) == 0)
    {
      base.xui.playerUI.windowManager.Open(XUiC_MainMenu.ID, _bModal: true);
    }
  }

  private void OpenOptions(string _optionsWindowName)
  {
    continueGamePause = true;
    base.xui.playerUI.windowManager.Close(windowGroup.ID);
    base.xui.playerUI.windowManager.Open(_optionsWindowName, _bModal: true);
  }

  public override void OnOpen()
  {
    base.OnOpen();
    IsDirty = true;
    continueGamePause = false;
    windowGroup.openWindowOnEsc = ((GameStats.GetInt(EnumGameStats.GameState) == 0) ? XUiC_MMII_MainMenu.ID : null);
    RefreshBindings();
  }

  public override void OnClose()
  {
    base.OnClose();
    if (!continueGamePause && GameStats.GetInt(EnumGameStats.GameState) == 2)
    {
      GameManager.Instance.Pause(_bOn: false);
    }
  }

  public override void Update(float _dt)
  {
    base.Update(_dt);
    if (IsDirty)
    {
      RefreshBindings();
      IsDirty = false;
    }
  }

  public override bool GetBindingValue(ref string _value, BindingItem _binding)
  {
    switch (_binding.FieldName)
    {
      case "ingame":
        _value = (GameStats.GetInt(EnumGameStats.GameState) != 0).ToString();
        return true;
      case "notingame":
        _value = (GameStats.GetInt(EnumGameStats.GameState) == 0).ToString();
        return true;
      case "notreleaseingame":
        _value = "false";
        return true;
      default:
        return base.GetBindingValue(ref _value, _binding);
    }
  }
}
