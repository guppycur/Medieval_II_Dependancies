using UnityEngine;

public class XUiC_MMII_MainMenu : XUiController
{
	public static string ID = "MMII_MainMenu";

	private XUiC_SimpleButton btnNewGame;
	private XUiC_SimpleButton btnContinueGame;
	private XUiC_SimpleButton btnConnectToServer;
	private XUiC_SimpleButton btnEditingTools;
	private XUiC_SimpleButton btnOptions;
	private XUiC_SimpleButton btnCredits;
	public XUiC_SimpleButton btnQuit;

  public XUiC_SimpleButton btn_MMII_Options;

  public XUiC_MMII_MainMenu() : base()
  {

  }

  public override void Init()
	{
		base.Init();

		ID = WindowGroup.ID;
		btnNewGame = (GetChildById("btnNewGame") as XUiC_SimpleButton);
    btnNewGame.OnPressed += btnNewGame_OnPressed;
    btnContinueGame = (GetChildById("btnContinueGame") as XUiC_SimpleButton);
    btnContinueGame.OnPressed += btnContinueGame_OnPressed;
    btnConnectToServer = (GetChildById("btnConnectToServer") as XUiC_SimpleButton);
    btnConnectToServer.OnPressed += btnConnectToServer_OnPressed;
    btnEditingTools = (GetChildById("btnEditingTools") as XUiC_SimpleButton);
    btnEditingTools.OnPressed += btnEditingTools_OnPressed;
    btnOptions = (GetChildById("btnOptions") as XUiC_SimpleButton);
    btnOptions.OnPressed += btnOptions_OnPressed;
    btnCredits = (GetChildById("btnCredits") as XUiC_SimpleButton);
    btnCredits.OnPressed += btnCredits_OnPressed;
    btnQuit = (GetChildById("btnQuit") as XUiC_SimpleButton);
    btnQuit.OnPressed += btnQuit_OnPressed;

    btn_MMII_Options = (GetChildById("btnMMIIOptions") as XUiC_SimpleButton);
    btn_MMII_Options.OnPressed += btnMMIIOptions_OnPressed;
  }

  private void btnNewGame_OnPressed(XUiController _sender, OnPressEventArgs _e)
	{
		XUiC_NewContinueGame.SetIsContinueGame(base.xui, _continueGame: false);
		CheckProfile(XUiC_NewContinueGame.ID);
	}

	private void btnContinueGame_OnPressed(XUiController _sender, OnPressEventArgs _e)
	{
		XUiC_NewContinueGame.SetIsContinueGame(base.xui, _continueGame: true);
		CheckProfile(XUiC_NewContinueGame.ID);
	}

	private void btnConnectToServer_OnPressed(XUiController _sender, OnPressEventArgs _e)
	{
		CheckProfile(XUiC_ServerBrowser.ID);
	}

	private void btnEditingTools_OnPressed(XUiController _sender, OnPressEventArgs _e)
	{
		base.xui.playerUI.windowManager.Close(windowGroup.ID);
		base.xui.playerUI.windowManager.Open(XUiC_EditingTools.ID, _bModal: true);
	}

	private void btnOptions_OnPressed(XUiController _sender, OnPressEventArgs _e)
	{
		base.xui.playerUI.windowManager.Close(windowGroup.ID);
		base.xui.playerUI.windowManager.Open(XUiC_OptionsMenu.ID, _bModal: true);
	}

	private void btnCredits_OnPressed(XUiController _sender, OnPressEventArgs _e)
	{
		base.xui.playerUI.windowManager.Close(windowGroup.ID);
		base.xui.playerUI.windowManager.Open(XUiC_Credits.ID, _bModal: true);
	}

  private void btnMMIIOptions_OnPressed(XUiController _sender, OnPressEventArgs _e)
  {
    base.xui.playerUI.windowManager.Close(windowGroup.ID);
    base.xui.playerUI.windowManager.Open(XUiC_MMII_ModOptions.ID, _bModal: true);
  }

  private void btnQuit_OnPressed(XUiController _sender, OnPressEventArgs _e)
	{
		Application.Quit();
	}

	private void CheckProfile(string _windowToOpen)
	{
		ProfileSDF profileSDF = new ProfileSDF();
		base.xui.playerUI.windowManager.Close(windowGroup.ID);

		if (profileSDF.GetSelectedProfile().Length == 0)
		{
			XUiC_OptionsProfiles.Open(base.xui, delegate
			{
				base.xui.playerUI.windowManager.Open(_windowToOpen, _bModal: true);
			});
		}
		else
		{
			base.xui.playerUI.windowManager.Open(_windowToOpen, _bModal: true);
		}
	}

	public override void Update(float _dt)
	{
		base.Update(_dt);
		windowGroup.isEscClosable = false;
	}

	public override void OnOpen()
	{
		base.OnOpen();

		base.xui.playerUI.windowManager.Close("eacWarning");

		if (base.xui.playerUI.LocalPlayerInput != null)
		{
			base.xui.playerUI.LocalPlayerInput.Reset();
			base.xui.playerUI.LocalPlayerInput.Push(base.xui.playerUI.playerInput);
			if (base.xui.playerUI.windowManager.IsWindowOpen(GUIWindowConsole.ID))
			{
				base.xui.playerUI.LocalPlayerInput.Push(base.xui.playerUI.playerInput.GUIActions);
			}
		}

		btnConnectToServer.Enabled = (Steam.ApiStatus != Steam.EInitResult.OFFLINE);
		base.xui.playerUI.nguiWindowManager.Show(EnumNGUIWindow.MainMenuBackground, _bEnable: false);
		base.xui.playerUI.windowManager.Open("menuBackground", _bModal: false);
		string @string = GameStats.GetString(EnumGameStats.GlobalMessageToShow);
		GameStats.Set(EnumGameStats.GlobalMessageToShow, string.Empty);

		if (!string.IsNullOrEmpty(@string))
		{
			GUIWindowMessageBox w = (GUIWindowMessageBox)base.xui.playerUI.windowManager.GetWindow(GUIWindowMessageBox.ID);
			base.xui.playerUI.windowManager.Open(w, _bModal: true);
		}

		base.xui.playerUI.windowManager.Open("mainMenuLogo", _bModal: false);
	}

	public override void OnClose()
	{
		base.OnClose();

		base.xui.playerUI.windowManager.Close("mainMenuLogo");
	}
}
