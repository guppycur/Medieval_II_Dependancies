using System;
using UnityEngine;

public class XUiC_MMII_ModOptions : XUiController
{
  public static string ID = "MMII_ModOptions";
  public static event Action OnSettingsChanged;

  private GameObject headerTemplate;
  private GameObject controlTemplate;
  private XUiC_TabSelector _Tabs;
  private XUiC_SimpleButton btnBack;
  private XUiC_SimpleButton btnDefaults;
  private XUiC_SimpleButton btnApply;

  // Options
  private XUiC_ComboBoxBool _CrosshairEnabled;
  private XUiC_ColorPicker _CrosshairColorPicker;

  public override void Init()
  {
    base.Init();

    headerTemplate = GetChildById("headerTemplate").ViewComponent.UiTransform.gameObject;
    controlTemplate = GetChildById("controlTemplate").ViewComponent.UiTransform.gameObject;
    _Tabs = GetChildById("tabs") as XUiC_TabSelector;

    btnBack = (GetChildById("btnBack") as XUiC_SimpleButton);
    btnBack.OnPressed += BtnBack_OnPressed;

    btnApply = (GetChildById("btnApply") as XUiC_SimpleButton);
    btnApply.Enabled = false;
    btnApply.OnPressed += BtnApply_OnPressed;

    btnDefaults = (GetChildById("btnDefaults") as XUiC_SimpleButton);
    btnDefaults.OnPressed += BtnDefaults_OnOnPressed;

    _CrosshairEnabled = GetChildById("CrosshairEnabled").GetChildByType<XUiC_ComboBoxBool>();
    _CrosshairEnabled.Value = ModPrefs.Instance.CrosshairEnabled;
    _CrosshairEnabled.OnValueChangedGeneric += Combo_OnValueChangedGeneric;

    _CrosshairColorPicker = GetChildById("CrosshairColorPicker").GetChildByType<XUiC_ColorPicker>();
    _CrosshairColorPicker.OnSelectedColorChanged += CrosshairColorPicker_OnSelectedColorChanged;
  }

  private void Combo_OnValueChangedGeneric(XUiController _sender)
  {
    btnApply.Enabled = true;
  }

  private void BtnApply_OnPressed(XUiController _sender, OnPressEventArgs _e)
  {
    ApplyChanges();
    btnApply.Enabled = false;
  }

  private void BtnDefaults_OnOnPressed(XUiController _sender, OnPressEventArgs _onPressEventArgs)
  {
    SetDefaults();
    btnApply.Enabled = true;
  }

  private void BtnBack_OnPressed(XUiController _sender, OnPressEventArgs _e)
  {
    xui.playerUI.windowManager.Close(windowGroup.ID);
    if (GameStats.GetInt(EnumGameStats.GameState) == 0)
    {
      xui.playerUI.windowManager.Open(XUiC_MMII_MainMenu.ID, _bModal: true);
    }
  }

  private void RefreshOptions()
  {
    _CrosshairEnabled.Value = ModPrefs.Instance.CrosshairEnabled;
    _CrosshairColorPicker.SelectedColor = ModPrefs.Instance.CrosshairColor;
  }

  private void SetDefaults()
  {
    _CrosshairEnabled.Value = true;
    _CrosshairColorPicker.SelectedColor = Color.white;
  }

  private void ApplyChanges()
  {
    ModPrefs.Instance.CrosshairEnabled = _CrosshairEnabled.Value;
    ModPrefs.Instance.CrosshairColor = _CrosshairColorPicker.SelectedColor;

    if (XUiC_MMII_ModOptions.OnSettingsChanged != null)
    {
      XUiC_MMII_ModOptions.OnSettingsChanged();
    }
  }

  public override void OnOpen()
  {
    SetEscapeWindow();
    RefreshOptions();
    _Tabs.SelectedTabIndex = 0;

    base.OnOpen();

    headerTemplate.SetActive(value: false);
    controlTemplate.SetActive(value: false);
  }

  public override void OnClose()
  {
    base.OnClose();

    if (GameStats.GetInt(EnumGameStats.GameState) != 0)
      GameManager.Instance.Pause(false);

    btnApply.Enabled = false;
  }

  public override void Update(float _dt)
  {
    base.Update(_dt);

    if (IsDirty)
    {
      RefreshBindings();
      IsDirty = false;
    }
  }

  public override bool GetBindingValue(ref string _value, BindingItem _binding)
  {
    switch (_binding.FieldName)
    {
      case "somenewbinding":
        _value = "stuff";
        return true;

      default:
        return base.GetBindingValue(ref _value, _binding);
    }
  }

  private void SetEscapeWindow()
  {
    WindowGroup.openWindowOnEsc = (GameStats.GetInt(EnumGameStats.GameState) == 0) ? XUiC_MMII_MainMenu.ID : "";
  }

  private void CrosshairColorPicker_OnSelectedColorChanged(Color selColor)
  {
    btnApply.Enabled = true;
  }
}
