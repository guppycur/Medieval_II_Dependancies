using System;
using System.IO;
using System.Xml;
using UnityEngine;

public class ModPrefs
{
  static readonly Lazy<ModPrefs> _Inst = new Lazy<ModPrefs>(() => new ModPrefs());
  public static ModPrefs Instance { get => _Inst.Value; }

  readonly string ModFolder = "MMII_ModPrefs";
  public readonly string ModPrefsXml = "MedievalModII_ModPrefs.xml";
  string _file;

  // Preferences
  public bool CrosshairEnabled { get; set; } = true;
  public Color CrosshairColor { get; set; } = Color.white;

  private ModPrefs()
  {
  }

  public void Init()
  {
    Log.Out("Medieval Mod II - init preferences...");

    XUiC_MMII_ModOptions.OnSettingsChanged += OnModPrefsChanged;
    _file = Path.GetFullPath(Path.Combine(Utils.GetGameDir("Data/Config"), ModPrefsXml));
    bool fileCheck = File.Exists(_file);
    Log.Out($"File found: {fileCheck} - {_file}");

    if (!fileCheck)
    {
      string modPath = Path.GetFullPath(Path.Combine(Utils.GetGamePath(), "Mods", ModFolder, "Config", ModPrefsXml));
      fileCheck = File.Exists(modPath);
      Log.Out($"File found: {fileCheck} - {modPath}");
      if (!fileCheck)
      {
        Log.Warning($"Failed to locate {ModPrefsXml} in all locations.  Using ModPrefs default settings.");
        SaveXml();
        return;
      }
      else
      {
        Log.Out("Copying files over");
        File.Copy(modPath, _file);
      }
    }

    XmlDocument xmlDocument = new XmlDocument();
    try
    {
      xmlDocument.Load(_file);
      var x = xmlDocument.DocumentElement;

      if (bool.TryParse(x.SelectSingleNode("CrosshairEnabled/@value").Value, out bool result))
        CrosshairEnabled = result;

      if (ColorUtility.TryParseHtmlString(x.SelectSingleNode("CrosshairColor/@value").Value, out Color color))
        CrosshairColor = color;
    }
    catch (Exception e)
    {
      Log.Error($"Failed to initialize Medieval Mod II preferences!  Check your MedievalModII_ModPrefs.xml for errors.");
      Log.Exception(e);
    }

    Log.Out("Medieval Mod II - init preferences complete.");
  }

  private void OnModPrefsChanged()
  {
    Log.Out("ModPrefs changed.");

    XmlDocument xmlDocument = new XmlDocument();

    using (var fs = File.Open(_file, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
    {
      xmlDocument.Load(fs);
    }

    var x = xmlDocument.DocumentElement;

    x.SelectSingleNode("CrosshairEnabled/@value").Value = CrosshairEnabled.ToString();
    x.SelectSingleNode("CrosshairColor/@value").Value = "#" + ColorUtility.ToHtmlStringRGBA(CrosshairColor);

    using (var fs = File.Open(_file, FileMode.Create))
    {
      xmlDocument.Save(fs);
    }
  }

  public void SetDefaults()
  {
    CrosshairEnabled = true;
    CrosshairColor = Color.white;
  }

  private void SaveXml()
  {
    XmlDocument xmlDocument = new XmlDocument();
    XmlDeclaration xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "UTF-8", null);
    XmlElement root = xmlDocument.DocumentElement;
    xmlDocument.InsertBefore(xmlDeclaration, root);

    var main = xmlDocument.CreateElement(string.Empty, "MMII_ModPrefs", string.Empty);
    xmlDocument.AppendChild(main);

    var crosshairEnabled = xmlDocument.CreateElement(string.Empty, "CrosshairEnabled", string.Empty);
    crosshairEnabled.SetAttribute("value", "true");
    main.AppendChild(crosshairEnabled);

    var crosshairColor = xmlDocument.CreateElement(string.Empty, "CrosshairColor", string.Empty);
    crosshairColor.SetAttribute("value", "#FFFFFFFF");
    main.AppendChild(crosshairColor);

    using (var fs = File.Open(_file, FileMode.Create))
    {
      xmlDocument.Save(fs);
    }

  }
}
