using DMT;
using HarmonyLib;
using System;
using UnityEngine;


[HarmonyPatch(typeof(AvatarAnimalController))]
[HarmonyPatch("LateUpdate")]
public class SphereII_AvatarAnimalController
{
    static void Postfix(AvatarAnimalController __instance, EntityAlive ___entity)
    {
//       Debug.Log("AvatarAnimalController: Entity Speed Forward: " + ___entity.speedForward + "   Strafe: " + ___entity.speedStrafe);
    }
}

[HarmonyPatch(typeof(AvatarZombie01Controller))]
[HarmonyPatch("LateUpdate")]
public class SphereII_AvatarZombie01Controller
{
    static void Postfix(AvatarZombie01Controller __instance, EntityAlive ___entity)
    {
//       Debug.Log("AvatarZombie01Controller: Entity Speed Forward: " + ___entity.speedForward);
//	   Debug.Log("AvatarZombie01Controller: Entity Speed Forward: " + ___entity.speedForward + "   Strafe: " + ___entity.speedStrafe);
    }
}
//public class SphereII_AnimatorMapper
//{
//    private static string AdvFeatureClass = "AdvancedTroubleshootingFeatures";
//    private static string Feature = "AnimatorMapper";

//    [HarmonyPatch(typeof(AvatarController))]
//    [HarmonyPatch("SetTrigger")]
//    [HarmonyPatch(new Type[] { typeof(string) })]
//    public class SphereII_AnimatorMapperTrigger
//    {
//        public static bool Prefix(AvatarController __instance, string _property)
//        {
//            if (!Configuration.CheckFeatureStatus(AdvFeatureClass, Feature))
//                return true;

//            AdvLogging.DisplayLog(AdvFeatureClass, "Set Trigger(): " + _property);
//            return true;
//        }
//    }

//}

//[HarmonyPatch(typeof(XmlPatcher))]
//[HarmonyPatch("singlePatch")]
//public class SphereII_XmlPatcher_SinglePatch
//{

//        static bool Prefix(XmlFile _targetFile, XmlElement _patchElement, string _patchName)
//        {
//            if (!Configuration.CheckFeatureStatus(AdvFeatureClass, Feature))
//            return true;