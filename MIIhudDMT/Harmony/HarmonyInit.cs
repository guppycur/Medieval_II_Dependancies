﻿using DMT;
using HarmonyLib;
using System.Reflection;

namespace TormentedEmu_Mods_A19
{
  public class TE_MMIIHudDmtInit : IHarmony
  {
    public void Start()
    {
      var harmony = new Harmony("TormentedEmu.MMIIHudDmt.Mods.A19");
      harmony.PatchAll(Assembly.GetExecutingAssembly());
    }
  }
}
