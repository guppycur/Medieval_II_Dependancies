﻿using HarmonyLib;

namespace TormentedEmu_Mods_A19
{
  public class Harmony_TE_CollectedItemControllerPatch
  {
    [HarmonyPatch(typeof(XUiC_CollectedItemList), "AddIconNotification")]
    public class XUiC_CollectedItemList_AddIconNotification
    {
      static bool Prefix(ref XUiC_CollectedItemList __instance, string iconNotifier, int count, bool _bAddOnlyIfNotExisting)
      {
        return false;
      }
    }

    [HarmonyPatch(typeof(XUiC_CollectedItemList), "AddItemStack")]
    public class XUiC_CollectedItemList_AddItemStack
    {
      static bool Prefix(ref XUiC_CollectedItemList __instance, ItemStack _is, bool _bAddOnlyIfNotExisting)
      {
        return false;
      }
    }
  }
}
