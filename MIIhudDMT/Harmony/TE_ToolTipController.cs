﻿using HarmonyLib;
using System;

namespace TormentedEmu_Mods_A19
{
  public class Harmony_TE_ToolTipLineController
  {
    [HarmonyPatch(typeof(NGuiWdwInGameHUD), "SetTooltipText", new Type[] { typeof(string), typeof(string[]), typeof(string), typeof(ToolTipEvent) })]
    public class NGuiWdwInGameHUD_SetTooltipText
    {
      static bool Prefix(ref NGuiWdwInGameHUD __instance, ref string _text, ref string[] _args, ref string _alertSound, ref ToolTipEvent eventHandler)
      {
        // Nuke all incoming tooltip texts and display nothing!
        return false;
      }
    }
  }
}
