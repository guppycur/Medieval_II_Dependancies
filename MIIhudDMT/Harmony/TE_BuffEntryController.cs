﻿using HarmonyLib;

namespace TormentedEmu_Mods_A19
{
  public class Harmony_TE_BuffEntryController
  {
    [HarmonyPatch(typeof(XUiC_ActiveBuffEntry), "GetBindingValue")]
    public class XUiC_ActiveBuffEntry_GetBindingValue
    {
      static bool Prefix(ref bool __result, ref XUiC_ActiveBuffEntry __instance, ref string value, BindingItem binding, string ___buffName)
      {
        if (binding.FieldName == "hasbuff")
        {
          if (string.IsNullOrEmpty(___buffName))
            value = "false";
          else
            value = "true";

          __result = true;
          return false;
        }

        return true;
      }
    }
  }
}
