﻿using HarmonyLib;
using System.Collections;

namespace TormentedEmu_Mods_A19
{
  public class Harmony_TE_QuestTrackerPatch
  {
    [HarmonyPatch(typeof(Quest), "trackLater")]
    public class Quest_trackLater
    {
      static bool Prefix(ref IEnumerator __result, ref Quest __instance)
      {
        __instance.Tracked = false;
        __result = StopAutoTracked();

        return false;
      }

      static IEnumerator StopAutoTracked()
      {
        yield break;
      }
    }
  }
}
