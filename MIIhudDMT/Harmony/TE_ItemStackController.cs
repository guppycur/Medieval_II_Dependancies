using HarmonyLib;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace TormentedEmu_Mods_A19
{
  public class Harmony_TE_ItemStackController
  {
    [HarmonyPatch(typeof(XUiC_ItemStack), "GetBindingValue")]
    public class XUiC_ItemStack_GetBindingValue
    {
      static bool Prefix(ref bool __result, ref XUiC_ItemStack __instance, ref string value, BindingItem binding, ItemClass ___itemClass)
      {
        if (binding.FieldName == "hasitemcount")
        {
          if (__instance.ItemStack.count > 0 && ___itemClass != null && ___itemClass.Stacknumber.Value > 1)
            value = "true";
          else
            value = "false";

          __result = true;
          return false;
        }

        return true;
      }
    }

    [HarmonyPatch(typeof(XUiC_ItemStack), "Update")]
    public class XUiC_ItemStack_Update
    {
      static XUiC_ItemStack _ItemStack;
      static MethodInfo _SetSpriteName = SymbolExtensions.GetMethodInfo(() => SetSpriteName(ref _ItemStack));

      static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
      {
        var codes = new List<CodeInstruction>(instructions);

        for (int index = 0; index < codes.Count; index++)
        {
          if (codes[index].opcode == OpCodes.Callvirt && codes[index].operand.ToString().Contains("HasMods") &&
              codes[++index].opcode == OpCodes.Brfalse_S &&
              codes[++index].opcode == OpCodes.Ldarg_0)
          {
            codes[index++] = new CodeInstruction(OpCodes.Ldarga, 0);
            codes[index++] = new CodeInstruction(OpCodes.Call, _SetSpriteName);
            codes[index++] = new CodeInstruction(OpCodes.Nop);
            codes[index] = new CodeInstruction(OpCodes.Nop);
            break;
          }
        }

        return codes.AsEnumerable();
      }

      static void SetSpriteName(ref XUiC_ItemStack item)
      {
        if (item.StackLocation == XUiC_ItemStack.StackLocationTypes.ToolBelt)
          item.lockTypeIcon.SpriteName = "miihud_ui_game_symbol_modded"; // ***** CHANGE ME! *****
        else
          item.lockTypeIcon.SpriteName = "ui_game_symbol_modded";
      }
    }
  }
}
