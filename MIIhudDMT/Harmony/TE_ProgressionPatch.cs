﻿using HarmonyLib;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;

namespace TormentedEmu_Mods_A19
{
  public class Harmony_TE_ProgressionPatch
  {
    [HarmonyPatch(typeof(Progression), "AddLevelExp")]
    public class Progression_AddLevelExp
    {
      static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
      {
        var codes = new List<CodeInstruction>(instructions);
        int startIndex = -1;
        int endIndex = -1;
        bool foundStr = false;

        for (int index = 0; index < codes.Count; index++)
        {
          if (codes[index].opcode == OpCodes.Ldarg_0 &&
              codes[index + 1].opcode == OpCodes.Ldfld && codes[index + 1].operand.ToString().Contains("EntityAlive parent") &&
              codes[index + 2].opcode == OpCodes.Isinst && codes[index + 2].operand.ToString().Contains("EntityPlayerLocal"))
          {

            for (int end = index + 3; index < codes.Count; end++)
            {
              if (!foundStr && codes[end].opcode == OpCodes.Ldstr && codes[end].operand.ToString().Contains("ui_game_symbol_xp"))
              {
                foundStr = true;
                continue;
              }

              if (codes[end].opcode == OpCodes.Callvirt && codes[end].operand.ToString().Contains("AddIconNotification") && foundStr)
              {
                startIndex = index;
                endIndex = end;
                break;
              }
            }

            if (endIndex > -1)
              break;
          }
        }

        if (startIndex > -1 && endIndex > -1)
        {
          codes.RemoveRange(startIndex, endIndex - startIndex + 1);
        }

        return codes.AsEnumerable();
      }
    }
  }
}
