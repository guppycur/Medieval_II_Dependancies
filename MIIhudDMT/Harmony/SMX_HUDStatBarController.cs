using DMT;
using HarmonyLib;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SMX_HUDStatBar_Fixes
{
	//	Author: Sirillion - You are allowed to use the code within this file as you see fit as long as this line is not removed or changed.

	//	Class & Method definition
	[HarmonyPatch(typeof(XUiC_HUDStatBar))]
	[HarmonyPatch("GetBindingValue")]
	public class SMX_XUiC_HUDStatBar_Fix1
	{
		public static void Postfix(ref XUiC_HUDStatBar __instance, ref bool __result, ref string value, ref BindingItem binding, ref int ___currentAmmoCount, ref CachedStringFormatter<int> ___statcurrentFormatterInt)
		{
			string fieldName = binding.FieldName;
			if (fieldName == "statmax")
			{
				value = ___statcurrentFormatterInt.Format(___currentAmmoCount);
				__result = true;
			}
		}
	}
}